import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PinnwandComponent } from './pinnwand.component';

describe('PinnwandComponent', () => {
  let component: PinnwandComponent;
  let fixture: ComponentFixture<PinnwandComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PinnwandComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PinnwandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
