import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KanustationComponent } from './kanustation.component';

describe('KanustationComponent', () => {
  let component: KanustationComponent;
  let fixture: ComponentFixture<KanustationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KanustationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KanustationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
