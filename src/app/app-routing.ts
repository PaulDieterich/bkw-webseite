import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { BilderComponent } from './bilder/bilder.component';
import { LogonComponent } from './logon/logon.component';
import { UeberunsComponent } from './ueberuns/ueberuns.component';
import { GeschichteComponent } from './geschichte/geschichte.component';
import { UnserProgrammComponent } from './unserProgramm/unserProgramm.component';
import { NeuesvombkwComponent } from './neuesvombkw/neuesvombkw.component';
import { JugendseiteComponent } from './jugendseite/jugendseite.component';
import { MitgliederComponent } from './mitglieder/mitglieder.component';
import { KanustationComponent } from './kanustation/kanustation.component';
import { ZustaendigkeitenComponent } from './zustaendigkeiten/zustaendigkeiten.component';
import { VereinsbooteComponent } from './vereinsboote/vereinsboote.component';
import { SicherheitComponent } from './sicherheit/sicherheit.component';
import { FotoalbumComponent } from './fotoalbum/fotoalbum.component';
import { TourenComponent } from './touren/touren.component';
import { ArchivComponent } from './archiv/archiv.component';
import { WetterComponent } from './wetter/wetter.component';
import { TiedeComponent } from './tiede/tiede.component';
import { InternComponent } from './intern/intern.component';
import { InternBereichComponent } from './intern-bereich/intern-bereich.component';
import { PinnwandComponent } from './pinnwand/pinnwand.component';
import { FahrtenbuchComponent } from './fahrtenbuch/fahrtenbuch.component';
import { ImpressumComponent } from './impressum/impressum.component';
import { KanukurseimbkwComponent } from './kanukurseimbkw/kanukurseimbkw.component';
import { MitgliedwerdenComponent } from './mitgliedwerden/mitgliedwerden.component';

const appRoutes: Routes = [
    { path: 'Home', component: HomeComponent },
    { path: 'Bilder', component: BilderComponent },
    { path: 'Archiv', component: ArchivComponent },
    { path: 'Fahrtenbuch', component: FahrtenbuchComponent },
    { path: 'Fotoalbum', component: FotoalbumComponent },
    { path: 'Geschichte', component: GeschichteComponent },
    { path: 'Impressum', component: ImpressumComponent },
    { path: 'Intern', component: InternComponent },
    { path: 'Jugendseite', component: JugendseiteComponent },
    { path: 'DKVKanustation', component: KanustationComponent },
    { path: 'Logon', component: LogonComponent },
    { path: 'Mitgleidwerden', component: MitgliedwerdenComponent },
    { path: 'Mitglieder', component: MitgliederComponent },
    { path: 'Neuesvombkw', component: NeuesvombkwComponent },
    { path: 'Pinnwand', component: PinnwandComponent },
    { path: 'Tiede', component: TiedeComponent },
    { path: 'Touren', component: TourenComponent },
    { path: 'Ueberuns', component: UeberunsComponent },
    { path: 'UnserProgramm', component: UnserProgrammComponent },
    { path: 'Vereinsboote', component: VereinsbooteComponent },
    { path: 'Wetter', component: WetterComponent },
    { path: 'Zustaendigkeiten', component: ZustaendigkeitenComponent },
    { path: 'Kanukurseimbkw', component: KanukurseimbkwComponent },
    { path: 'Sicherheit', component: SicherheitComponent },
    {path: 'InternBereich', component: InternBereichComponent},
    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);