import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TiedeComponent } from './tiede.component';

describe('TiedeComponent', () => {
  let component: TiedeComponent;
  let fixture: ComponentFixture<TiedeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TiedeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TiedeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
