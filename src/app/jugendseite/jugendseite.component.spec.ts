import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JugendseiteComponent } from './jugendseite.component';

describe('JugendseiteComponent', () => {
  let component: JugendseiteComponent;
  let fixture: ComponentFixture<JugendseiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JugendseiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JugendseiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
