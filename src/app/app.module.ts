import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule} from '@angular/common/http';

import { routing }        from './app-routing';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { BilderComponent } from './bilder/bilder.component';
import { LogonComponent } from './logon/logon.component';
import { GeschichteComponent } from './geschichte/geschichte.component';
import { UnserProgrammComponent } from './unserProgramm/unserProgramm.component';
import { JugendseiteComponent } from './jugendseite/jugendseite.component';
import { MitgliederComponent } from './mitglieder/mitglieder.component';
import { KanustationComponent } from './kanustation/kanustation.component';
import { ZustaendigkeitenComponent } from './zustaendigkeiten/zustaendigkeiten.component';
import { VereinsbooteComponent } from './vereinsboote/vereinsboote.component';
import { SicherheitComponent } from './sicherheit/sicherheit.component';
import { FotoalbumComponent } from './fotoalbum/fotoalbum.component';
import { TourenComponent } from './touren/touren.component';
import { ArchivComponent } from './archiv/archiv.component';
import { WetterComponent } from './wetter/wetter.component';
import { TiedeComponent } from './tiede/tiede.component';
import { InternComponent } from './intern/intern.component';
import { PinnwandComponent } from './pinnwand/pinnwand.component';
import { FahrtenbuchComponent } from './fahrtenbuch/fahrtenbuch.component';
import { ImpressumComponent } from './impressum/impressum.component';
import { UeberunsComponent } from './ueberuns/ueberuns.component';
import { NeuesvombkwComponent } from './neuesvombkw/neuesvombkw.component';
import { KanukurseimbkwComponent } from './kanukurseimbkw/kanukurseimbkw.component';
import { MitgliedwerdenComponent } from './mitgliedwerden/mitgliedwerden.component';
import { InternBereichComponent } from './intern-bereich/intern-bereich.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    BilderComponent,
    BilderComponent,
    LogonComponent,
    GeschichteComponent,
    UnserProgrammComponent,
    JugendseiteComponent,
    MitgliederComponent,
    KanustationComponent,
    ZustaendigkeitenComponent,
    VereinsbooteComponent,
    SicherheitComponent,
    FotoalbumComponent,
    TourenComponent,
    ArchivComponent,
    WetterComponent,
    TiedeComponent,
    InternComponent,
    PinnwandComponent,
    FahrtenbuchComponent,
    ImpressumComponent,
    UeberunsComponent,
    NeuesvombkwComponent,
    KanukurseimbkwComponent,
    MitgliedwerdenComponent,
    InternBereichComponent
  ],
  imports: [
    routing,
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
