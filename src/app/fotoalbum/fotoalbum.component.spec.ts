import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FotoalbumComponent } from './fotoalbum.component';

describe('FotoalbumComponent', () => {
  let component: FotoalbumComponent;
  let fixture: ComponentFixture<FotoalbumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FotoalbumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FotoalbumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
