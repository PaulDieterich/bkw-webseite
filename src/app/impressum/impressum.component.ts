import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-impressum',
  templateUrl: './impressum.component.html',
  styleUrls: ['./impressum.component.css']
})
export class ImpressumComponent implements OnInit {
  name =" Bremer Kanu-Wanderer e.V. ";
  anschrift = "Richard-Jürgens-Weg 5";
  Plz ="28205";
  ort ="bremen";
  constructor() { }

  ngOnInit() {
  }

}
