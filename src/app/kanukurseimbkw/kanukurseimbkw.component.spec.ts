import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KanukurseimbkwComponent } from './kanukurseimbkw.component';

describe('KanukurseimbkwComponent', () => {
  let component: KanukurseimbkwComponent;
  let fixture: ComponentFixture<KanukurseimbkwComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KanukurseimbkwComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KanukurseimbkwComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
