import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VereinsbooteComponent } from './vereinsboote.component';

describe('VereinsbooteComponent', () => {
  let component: VereinsbooteComponent;
  let fixture: ComponentFixture<VereinsbooteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VereinsbooteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VereinsbooteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
