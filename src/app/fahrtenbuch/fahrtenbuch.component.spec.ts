import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FahrtenbuchComponent } from './fahrtenbuch.component';

describe('FahrtenbuchComponent', () => {
  let component: FahrtenbuchComponent;
  let fixture: ComponentFixture<FahrtenbuchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FahrtenbuchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FahrtenbuchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
